from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import os

from channels.models import Channel
# Create your models here.

class VideoCategory(models.Model):
    """
    Video types to be available for VideoWatch
    """
    video_type = models.CharField(max_length=150, default='others', unique=True)

    def __str__(self):
        return self.video_type
        
class Video(models.Model):

    """
    All videos available for the users
    """
    # def upload_to(instance, filename):
    #     now = timezone.now()
    #     base, extension = os.path.splitext(filename.lower())
    #     milliseconds = now.microsecond // 1000
    #     return f"users/{instance.pk}/{now:%Y%m%d%H%M%S}{milliseconds}{extension}"
  
    video = models.FileField(blank=True, default='')
    user = models.ForeignKey(User, related_name='user_videos', related_query_name='person', on_delete=models.SET_NULL, null=True)
    category = models.ForeignKey(VideoCategory, related_name='video_category', on_delete=models.SET_NULL, null=True)
    channel = models.ForeignKey(Channel, related_name='video_channel', on_delete=models.SET_NULL, null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    deleted_at = models.DateTimeField(null=True)
    active = models.BooleanField(default=1)
    description = models.TextField(null=True)
    short_description = models.CharField(max_length=150, null=True)

    def __str__(self):
        return f"{self.user.username}_{self.category.video_type}_{self.channel.channel_name}"


class LikedVideo(models.Model):
    """
    Contains all the liked videos by user
    """
    video = models.ForeignKey(Video, related_name='video_liked', on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, related_name='user_liked', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f"{self.user.username}"




