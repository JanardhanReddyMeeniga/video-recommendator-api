from django.contrib import admin
from videodetails.models import VideoCategory, Video

# Register your models here.

admin.site.register(VideoCategory)
admin.site.register(Video)
