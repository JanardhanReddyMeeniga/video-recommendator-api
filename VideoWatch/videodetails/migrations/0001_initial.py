# Generated by Django 3.1.7 on 2021-04-28 07:16

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('video_type', models.CharField(choices=[('Entertainment', 'Entertainment'), ('Education', 'Education'), ('Gaming', 'Gaming'), ('Other', 'Other')], default='others', max_length=150)),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('video', models.FileField(blank=True, default='', upload_to='')),
                ('uploaded_at', models.DateTimeField(auto_now_add=True)),
                ('deleted_at', models.DateTimeField()),
                ('active', models.BooleanField(default=1)),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='video_category', to='videodetails.videocategory')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user_videos', related_query_name='person', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
