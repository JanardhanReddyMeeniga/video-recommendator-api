from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from videodetails.models import Video

class VideoSerializer(ModelSerializer):
    uploaded_at = serializers.ReadOnlyField()
    class Meta:
        model = Video
        fields = ["video", "category", "uploaded_at"]

    def to_representation(self, instance):
        rep = super(VideoSerializer, self).to_representation(instance)
        rep['category'] = instance.category.video_type
        return rep