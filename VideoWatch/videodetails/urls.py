from django.urls import path

from videodetails.api.api_views import (
                                    VideoAPIView
                                )

app_name = "video"

urlpatterns = [
    path("", VideoAPIView.as_view(), name="vm_video")
]