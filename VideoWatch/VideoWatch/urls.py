"""VideoWatch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from rest_auth.registration.views import VerifyEmailView
from django.conf import settings
from django.conf.urls.static import static
from authentication.api.api_views import FacebookConnect, TwitterConnect, GoogleConnect

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Video API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('rest_auth.urls')),
    path('auth/registration/', include('rest_auth.registration.urls')),
    path('auth/facebook/connect/', FacebookConnect.as_view(), name='fb_connect'),
    path('auth/twitter/connect/', TwitterConnect.as_view(), name='twitter_connect'),
    path('auth/google/connect/', GoogleConnect.as_view(), name='google_login'),
    # path('auth/github/connect/', GithubConnect.as_view(), name='github_connect'),
    path(
        'reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'
    ),

    path('account-confirm-email/', VerifyEmailView.as_view(),
     name='account_email_verification_sent'),

    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),

    path('video/', include('videodetails.urls', namespace="video")),
    path('channel/', include('channels.urls', namespace="channel"))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


