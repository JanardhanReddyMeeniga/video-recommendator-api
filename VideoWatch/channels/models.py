from django.db import models

# Create your models here.

from django.contrib.auth.models import User


class Channel(models.Model):

    """
    User can have 'n' number of channels. 

    All the videos uploaded by users should be a 
    part of one of the channel
    """

    channel_name = models.CharField(max_length=150)
    user = models.ForeignKey(User, related_name='user_channel', related_query_name='person', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    description = models.TextField()
    short_description = models.CharField(max_length=150)
    active = models.BooleanField(default=1)

    def __str__(self):
        return f"{self.channel_name}_{self.user.username}"

class ChannelSubscription(models.Model):
    """
    A Channel can be subscribed by the 'n' number
    of users.
    """

    channel = models.ForeignKey(Channel, related_name='channel_subscription', related_query_name='channel_info', on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, related_name='user_channel_subs', related_query_name='person', on_delete=models.SET_NULL, null=True)
    subscribe = models.BooleanField(default=0)
    
    def __str__(self):
        return f"{self.channel.channel_name}_{self.user.username}"