from django.urls import path

from channels.api.api_views import (
                                    ChannelAPIView
                                )

app_name = "channel"

urlpatterns = [
    path("", ChannelAPIView.as_view(), name="vm_channel")
]