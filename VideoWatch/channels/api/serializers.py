from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from channels.models import Channel

class ChannelSerializer(ModelSerializer):

    class Meta:
        model = Channel
        fields = ["channel_name", "created_at", "description", "short_description"]


