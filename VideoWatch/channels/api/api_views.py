from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


from channels.models import Channel
from channels.api.serializers import ChannelSerializer

class ChannelAPIView(APIView):
    
    permission_classes = [IsAuthenticated]
    serializer_class = ChannelSerializer

    def post(self, request, format=None):
        serializer = ChannelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):

        channels = Channel.objects.filter(user=request.user, active=True)
        serializer = ChannelSerializer(channels, many=True)
        return Response(serializer.data)

class ChannelGetPutDeleteAPIView(APIView):
    def delete(self, request, format=None):
        pass
